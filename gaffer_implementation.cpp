#include <algorithm>
#include <chrono>
#include <iostream>
#include <iterator>
#include <optional>
#include <tuple>
#include <vector>

#include "SDL2pp/Exception.hh"
#include "SDL2pp/Optional.hh"
#include "SDL2pp/Renderer.hh"
#include "SDL2pp/SDL.hh"
#include "SDL2pp/SDLImage.hh"
#include "SDL2pp/Texture.hh"
#include "SDL2pp/Window.hh"

#include "SDL2/SDL.h"
#include "SDL2/SDL_events.h"
#include "SDL2/SDL_image.h"
#include "SDL2/SDL_render.h"

#include <cmath>
#include <cstdint>


constexpr int WINDOW_HEIGHT = 600;
constexpr int WINDOW_WIDTH = 800;

constexpr int SPRITE_HEIGHT = 112;
constexpr int SPRITE_WIDTH = 112;


using namespace std::literals;

using Clock = std::chrono::steady_clock;

auto constexpr dt = std::chrono::duration<long long, std::ratio<1, 60>>{1};

using duration = decltype(Clock::duration{} + dt);
using time_point = std::chrono::time_point<Clock, duration>;


struct State
{
    double acceleration = 1;  // m/s^2
    double velocity = 0;  // m/s
};


State operator+(State x, State y)
{
    return {x.acceleration + y.acceleration, x.velocity + y.velocity};
}


State operator*(State x, double y)
{
    return {x.acceleration * y, x.velocity * y};
}


void integrate(State& state,
    std::chrono::time_point<Clock, std::chrono::duration<double>>,
    std::chrono::duration<double> dt)
{
    using namespace std::literals;
    state.velocity += state.acceleration * dt / 1s;
};


void render(State state)
{
    using namespace std::chrono;

    static auto t = time_point_cast<seconds>(steady_clock::now());
    static int frame_count = 0;
    static int frame_rate = 0;
    
    auto pt = t;
    
    t = time_point_cast<seconds>(steady_clock::now());
    
    ++frame_count;

    if (t != pt) {
        frame_rate = frame_count;
        frame_count = 0;
    }

    // std::cout << "Frame rate is " << frame_rate << " frames per second\n";
}


int main(int argc, char **argv)
{
    SDL2pp::SDL sdl(SDL_INIT_VIDEO);

    SDL2pp::SDLImage sdl_image(IMG_INIT_PNG);

    SDL2pp::Window window("Gaffer Implementation 0.1.1",
        SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
        WINDOW_WIDTH, WINDOW_HEIGHT,
        SDL_WINDOW_SHOWN);

    SDL2pp::Renderer renderer(window, -1, SDL_RENDERER_ACCELERATED);

    time_point t{};

    time_point currentTime = Clock::now();
    duration accumulator = 0s;

    uint64_t ticks = 0;

    State previousState = {1, 0};
    State currentState = {1, 0};

    SDL2pp::Texture sprite(renderer, "assets/hex_red.png");

    bool quit = false;

    while (!quit) {
        SDL_Event event;

        while (SDL_PollEvent(&event)) {
            if (event.type == SDL_QUIT) {
                quit = true;
            }

            if (event.type == SDL_MOUSEBUTTONDOWN) {
                switch (event.button.button) {
                case SDL_BUTTON_LEFT:

                    break;
                case SDL_BUTTON_RIGHT:

                    break;
                default:
                    break;
                }
            } 
        }

        time_point newTime = Clock::now();
        auto frameTime = newTime - currentTime;

        if (frameTime > 250ms) frameTime = 250ms;

        currentTime = newTime;

        accumulator += frameTime;

        while (accumulator >= dt) {
            previousState = currentState;
            integrate(currentState, t, dt);

            t += dt;
            accumulator -= dt;

            ticks++;
        }

        const double alpha = std::chrono::duration<double>{accumulator} / dt;

        renderer.SetDrawColor(128, 128, 128);

        renderer.Clear();

        renderer.Copy(sprite, SDL2pp::NullOpt, SDL2pp::Rect(0, 0, 32, 28));

        renderer.Present();
    }

    return 0;
}
