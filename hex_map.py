def hex_map(index):
    v = index // 15 + index // 2

    if index % 2:
        result = (v + 1, v + 9, v + 10)
    else:
        result = (v, v + 1, v + 9)

    return result

if __name__ == "__main__":
    value_results = [
        (0, (0, 1, 9)),
        (1, (1, 9, 10)),
        (2, (1, 2, 10)),
        (3, (2, 10, 11)),
        (15, (9, 17, 18)),
        (16, (9, 10, 18)),
        (17, (10, 18, 19)),
        (30, (17, 18, 26)),
        (31, (18, 26, 27)),
        (45, (26, 34, 35)),
    ]

    for value, expected in value_results:
        result = hex_map(value)

        print(value, result, expected)

        assert result == expected

