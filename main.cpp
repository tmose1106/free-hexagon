#include <algorithm>
#include <array>
#include <iostream>
#include <limits>
#include <random>
#include <set>
#include <string>
#include <tuple>
#include <unordered_map>
#include <vector>

#include "SDL2/SDL.h"
#include "SDL2/SDL_image.h"

#include <cmath>


constexpr int SCREEN_WIDTH = 600;
constexpr int SCREEN_HEIGHT = 800;

constexpr int SCALE = 8;

constexpr int HEX_WIDTH = SCALE * 8;
constexpr int HEX_HEIGHT = SCALE * 7;

constexpr int BOARD_HEIGHT = 9;
constexpr int BOARD_COLUMN_COUNT = BOARD_HEIGHT * 2 - 1;

constexpr int BOARD_WIDTH = 5;

constexpr int BOARD_COUNT = BOARD_COLUMN_COUNT * BOARD_WIDTH;

constexpr int BOARD_INTERSECT_COUNT = (BOARD_COLUMN_COUNT - 2) * (BOARD_WIDTH * 2 - 1);


enum class SelectType {
  HexLeft,
  HexRight,
  Star,
  PearlUp,
  PearlDown
};


enum class SelectTexture {
  Hex,
  Star,
  Pearl
};


struct Selection {
  SelectType type;
  SDL_Rect position;
  int intersection_index;
  int hex_index;
};


enum class HexType {
  Red,
  Green,
  Blue,
  Yellow,
  Purple,
  Star,
  PearlDown,
  PearlUp
};


struct Hexagon {
  HexType type;
  SDL_Rect position;
  float rotation;
};


/* Load a surface using sdl_image and convert to a texture */
SDL_Texture *loadTexture(const char *file, SDL_Renderer *ren) {
  SDL_Surface *img_surface = nullptr;

  img_surface = IMG_Load(file);

  SDL_Texture *img_texture = nullptr;

  img_texture = SDL_CreateTextureFromSurface(ren, img_surface);

  SDL_FreeSurface(img_surface);

  return img_texture;
}


/* Load textures from strings in map */
template<typename Key>
auto loadTextureMap(std::unordered_map<Key, std::string> map, SDL_Renderer *ren)
{
  std::unordered_map<Key, SDL_Texture*> texture_map;

  for (const auto& [key, value] : map) {
    std::string path = "assets/" + value;
    texture_map[key] = loadTexture(path.c_str(), ren);
  }

  return texture_map;
}

/* NOTE: These functions below return vectors because order
 * actually matters.
 */

std::vector<int> intersectIndexToHexIndexs(int index)
{
  int value = index / 15 + index / 2;

  if ((index % 2) != 0) {
    return {value + 1, value + 9, value + 10};
  } else {
    return {value, value + 1, value + 9};
  }
}


std::vector<int> hexIndexToSurroundHexIndexs(int index) {
  return {index - 9, index - 1, index + 8, index + 9, index + 1, index - 8};
}


std::vector<int> hexIndexToYIndexes(int index) {
  return {index + 8, index + 1, index - 9};
}


std::vector<int> hexIndexToInvertedYIndexes(int index) {
  return {index - 8, index - 1, index + 9};
}


void rotateHexsClockwise(std::vector<Hexagon> *hexagons, const std::vector<int> *indices) {
  auto tmp_type = hexagons->at(indices->at(0)).type;

  for (int i = 0; i < indices->size() - 1; i++) {
    hexagons->at(indices->at(i)).type = hexagons->at(indices->at(i + 1)).type;
  }

  hexagons->at(indices->at(indices->size() - 1)).type = tmp_type;
}


/* Calculate all intersections on the board */
void fillHexIntersections(std::vector<std::pair<int, int>> *vec) {
  vec->resize(BOARD_INTERSECT_COUNT);

  for (int i = 0; i < 9; i++) {
    int x = (HEX_WIDTH * 3 / 4) + (HEX_WIDTH * 1.5) * (i / 2) + HEX_WIDTH * (i % 2);

    for (int j = 0; j < 8; j++) {
      int y = HEX_HEIGHT * (j + 1);

      vec->at(i * 15 + j * 2) = std::make_pair(x, y);
    }
  }

  for (int i = 0; i < 9; i++) {
    int x = HEX_WIDTH + (HEX_WIDTH * 1.5) * (i / 2) + (HEX_WIDTH / 2) * (i % 2);

    for (int j = 0; j < 7; j++) {
      int y = (HEX_HEIGHT / 2) + HEX_HEIGHT * (j + 1);

      vec->at(i * 15 + j * 2 + 1) = std::make_pair(x, y);
    }
  }
}


/* Fill vector with indices of internal (non-border) hexagons
 * for determining whether a special hexagon can be rotated
 */
void fillInternalHexIndices(std::vector<int> *vec) {
  for (int i = 0; i < BOARD_WIDTH - 1; i++) {
    int short_start = BOARD_HEIGHT + BOARD_COLUMN_COUNT * i + 1;

    for (int j = 0; j < BOARD_HEIGHT - 3; j++) {
      vec->push_back(short_start + j);
    }

    int tall_start = (BOARD_COLUMN_COUNT) * (i + 1) + 1;

    for (int k = 0; k < BOARD_HEIGHT - 2; k++) {
      vec->push_back(tall_start + k);
    }
  }
}


/* Fill vector with SDL_Rect structs, determining where each
 * hexagon is drawn
 */
void fillHexDestinationRectangles(std::vector<SDL_Rect> *vec) {
  int alt_y_offset = HEX_HEIGHT / 2;

  for (int i = 0; i < BOARD_WIDTH; i++) {
    int x_offset = HEX_WIDTH * 1.5 * i;

    for (int j = 0; j < BOARD_HEIGHT; j++) {
      vec->push_back({x_offset, HEX_HEIGHT * j, HEX_WIDTH, HEX_HEIGHT});
    }

    int alt_x_offset = x_offset + HEX_WIDTH * 0.75;

    for (int j = 0; j < BOARD_HEIGHT - 1; j++) {
      vec->push_back({alt_x_offset, HEX_HEIGHT * j + alt_y_offset, HEX_WIDTH, HEX_HEIGHT});
    }
  }
}


bool checkHexIndicesTypesMatch(std::vector<Hexagon> *hexagons, std::vector<int> hex_indices)
{
  std::vector<HexType> hex_types;

  // Using vector of indices, fill vector with associated types using a lambda
  std::transform(std::begin(hex_indices), std::end(hex_indices), std::back_inserter(hex_types), 
    [&hexagons](const int& hex_index) { return hexagons->at(hex_index).type; });

  // Check all hexagon types in vector match
  return std::adjacent_find(std::begin(hex_types), std::end(hex_types), std::not_equal_to<>()) == std::end(hex_types);
}


auto findHexMatches(std::vector<Hexagon> *hexagons, int intersect_count)
{
  std::vector<std::vector<int>> match_sets;
  
  for (int intersect_index = 0; intersect_index < intersect_count; intersect_index++) {
    std::vector<int> intersect_set = intersectIndexToHexIndexs(intersect_index);

    if (checkHexIndicesTypesMatch(hexagons, intersect_set)) {
      match_sets.push_back(intersect_set);
    }
  }

  return match_sets;
}


auto findSurroundMatches(std::vector<Hexagon> *hexagons, std::vector<int> *hex_indices)
{
  std::vector<int> match_indices;

  for (auto index : *hex_indices) {
    std::vector<int> surround_hex_indices = hexIndexToSurroundHexIndexs(index);

    if (checkHexIndicesTypesMatch(hexagons, surround_hex_indices)) {
      match_indices.push_back(index);
    }
  }

  return match_indices;
}


int main(int argc, char **argv)
{
  if (SDL_Init(SDL_INIT_VIDEO) < 0) {
    std::cout << "SDL could not initalize video! Error: "
      << SDL_GetError() << '\n';

    return 1;
  }

  if (IMG_Init(IMG_INIT_PNG) < 0) {
    std::cout << "SDL_image could not initalize! Error: "
      << IMG_GetError() << '\n';

    return 1;
  }

  SDL_Window *window = nullptr;

  window = SDL_CreateWindow(
    "Free Hexagon 0.1.0",
    SDL_WINDOWPOS_UNDEFINED,
    SDL_WINDOWPOS_UNDEFINED,
    SCREEN_WIDTH,
    SCREEN_HEIGHT,
    SDL_WINDOW_SHOWN);

  if (window == nullptr) {
    std::cout << "SDL could not create window! Error: "
      << SDL_GetError() << '\n';

    return 1;
  }

  SDL_Renderer *renderer = nullptr;

  renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);

  const std::unordered_map<HexType, std::string> hex_assets = {
    {HexType::Red, "hex_red.png"},
    {HexType::Green, "hex_green.png"},
    {HexType::Blue, "hex_blue.png"},
    {HexType::Yellow, "hex_yellow.png"},
    {HexType::Purple, "hex_purple.png"},
    {HexType::Star, "hex_white.png"},
    {HexType::PearlDown, "hex_pearl_up.png"},
    {HexType::PearlUp, "hex_pearl_down.png"},
  };

  auto hex_textures = loadTextureMap(hex_assets, renderer);

  const std::unordered_map<SelectTexture, std::string> select_assets = {
    {SelectTexture::Hex, "hex_select.png"},
    {SelectTexture::Star, "select_star.png"},
    {SelectTexture::Pearl, "select_pearl.png"},
  };

  auto select_textures = loadTextureMap(select_assets, renderer);

  std::vector<Hexagon> hexagons;

  std::random_device rd;
  std::mt19937 rng(rd());
  std::uniform_int_distribution<int> uni(0, 4);

  // Generate the hexagons
  {
    std::vector<SDL_Rect> hex_rects;

    fillHexDestinationRectangles(&hex_rects);

    for (int i = 0; i < BOARD_COUNT; i++) {
      auto hex_type = static_cast<HexType>(uni(rng));

      hexagons.push_back({hex_type, hex_rects[i], 0});
    }
  }

  // For testing the special hexagon types
  hexagons[2].type = HexType::Star;
  hexagons[10].type = HexType::Star;
  hexagons[39].type = HexType::PearlUp;
  hexagons[45].type = HexType::PearlDown;
  hexagons[46].type = HexType::Star;

  std::vector<int> internal_hex_indices;

  fillInternalHexIndices(&internal_hex_indices);

  std::vector<std::pair<int, int>> hex_intersects;

  fillHexIntersections(&hex_intersects);

  Selection selection;

  selection.type = SelectType::HexRight;
  selection.position = {0, 0, HEX_HEIGHT * 2, HEX_HEIGHT * 2};
  selection.intersection_index = 0;
  selection.hex_index = -1;

  bool changed = true;

  std::vector<std::pair<int, std::pair<int, int>>> special_positions;

  bool play;

  play = true;

  while (play) {
    SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
    SDL_RenderClear(renderer);

    for (auto hex : hexagons) {
      SDL_RenderCopy(renderer, hex_textures[hex.type], nullptr, &hex.position);
      // SDL_RenderPresent(renderer);
      // SDL_Delay(100);
    }

    if (selection.position.x >= 0 || selection.position.y >= 0) {
      switch(selection.type) {
        case SelectType::HexLeft:
          SDL_RenderCopyEx(renderer, select_textures[SelectTexture::Hex], nullptr, &selection.position, 180, nullptr, SDL_FLIP_VERTICAL);
          break;
        case SelectType::HexRight:
          SDL_RenderCopy(renderer, select_textures[SelectTexture::Hex], nullptr, &selection.position);
          break;
        case SelectType::Star:
          SDL_RenderCopy(renderer, select_textures[SelectTexture::Star], nullptr, &selection.position);
          break;
        case SelectType::PearlDown:
          SDL_RenderCopyEx(renderer, select_textures[SelectTexture::Pearl], nullptr, &selection.position, 180, nullptr, SDL_FLIP_HORIZONTAL);
          break;
        case SelectType::PearlUp:
          SDL_RenderCopy(renderer, select_textures[SelectTexture::Pearl], nullptr, &selection.position);
          break;
        default:
          break;
      }
    }

    SDL_RenderPresent(renderer);

    if (changed) {
      auto matches = findHexMatches(&hexagons, hex_intersects.size());

      // for (auto match : matches) {
      //   std::cout << "Found intersect: ";

      //   for (auto index : match) {
      //     std::cout << index << ' ';
      //   }

      //   std::cout << '\n';
      // }

      auto more_matches = findSurroundMatches(&hexagons, &internal_hex_indices);

      // for (auto match : more_matches) {
      //   std::cout << "Found surround at hex: " << match << '\n';
      // }

      special_positions.clear();

      for (int index : internal_hex_indices) {
        Hexagon hex = hexagons[index];

        if (hex.type == HexType::Star || hex.type == HexType::PearlDown || hex.type == HexType::PearlUp) {
          auto [x, y, w, h] = hex.position;

          special_positions.push_back(std::make_pair(index, std::make_pair(x + w / 2, y + h / 2)));
        }
      }

      changed = false;
    }

    SDL_Event event;

    while (SDL_PollEvent(&event)) {
      if (event.type == SDL_QUIT) {
        play = false;
      }

      if (event.type == SDL_MOUSEBUTTONUP) {
        std::vector<int> hex_indices;

        switch (selection.type) {
          case SelectType::HexLeft:
          case SelectType::HexRight:
            hex_indices = intersectIndexToHexIndexs(selection.intersection_index);
            break;
          case SelectType::Star:
            hex_indices = hexIndexToSurroundHexIndexs(selection.hex_index);
            break;
          case SelectType::PearlDown:
            hex_indices = hexIndexToYIndexes(selection.hex_index);
            break;
          case SelectType::PearlUp:
            hex_indices = hexIndexToInvertedYIndexes(selection.hex_index);
            break;
          default:
            break;
        }

        std::vector<int> hex_vec(std::begin(hex_indices), std::end(hex_indices));

        switch(event.button.button) {
          case SDL_BUTTON_RIGHT:
            std::reverse(std::begin(hex_vec), std::end(hex_vec));
            [[fallthrough]];
          case SDL_BUTTON_LEFT:
            rotateHexsClockwise(&hexagons, &hex_vec);
            break;
          default:
            break;
        }

        changed = true;
      }

      if (event.type == SDL_MOUSEMOTION) {
        int shortest_distance = std::numeric_limits<int>::max();
        std::pair<int, int> closest_intersect;

        int closest_index = -1;

        for (int i = 0; i < hex_intersects.size(); i++) {
          auto point = hex_intersects.at(i);

          int distance = std::pow(event.motion.x - point.first, 2)
            + std::pow(event.motion.y - point.second, 2);

          if (distance < shortest_distance) {
            shortest_distance = distance;
            closest_intersect = point;
            closest_index = i;
          }
        }

        int shortest_distance_to_hex = std::numeric_limits<int>::max();
        int closest_hex_index = -1;

        for (auto special : special_positions) {
          auto point = special.second;

          int distance = std::pow(event.motion.x - point.first, 2)
            + std::pow(event.motion.y - point.second, 2);

          if (distance < shortest_distance_to_hex) {
            shortest_distance_to_hex = distance;
            closest_hex_index = special.first;
          }
        }

        if (shortest_distance_to_hex < shortest_distance) {
          selection.hex_index = closest_hex_index;

          Hexagon closest_hex = hexagons[closest_hex_index];

          selection.position.x = closest_hex.position.x - 48;
          selection.position.y = closest_hex.position.y - 56;
          selection.position.w = HEX_WIDTH * 2.5;

          switch(closest_hex.type) {
            case HexType::Star:
              selection.type = SelectType::Star;
              selection.position.h = HEX_HEIGHT * 3;
              break;
            case HexType::PearlUp:
              selection.type = SelectType::PearlUp;
              selection.position.h = HEX_HEIGHT * 2.5;
              break;
            case HexType::PearlDown:
              selection.type = SelectType::PearlDown;
              selection.position.y = closest_hex.position.y - 28;
              selection.position.h = HEX_HEIGHT * 2.5;
              break;
            default:
              break;
          }
        } else {
          if (closest_index % 2) {
            selection.type = SelectType::HexLeft;
            selection.position.x = closest_intersect.first - 64;
          } else {
            selection.type = SelectType::HexRight;
            selection.position.x = closest_intersect.first - 48;
          }

          selection.position.y = closest_intersect.second - 56;
          selection.position.w = HEX_WIDTH * 1.75;
          selection.position.h = HEX_HEIGHT * 2;

          selection.intersection_index = closest_index;
        }
      }
    }
  }

  for (std::pair<HexType, SDL_Texture*> hex_texture : hex_textures) {
    SDL_DestroyTexture(hex_texture.second);
  }

  for (std::pair<SelectTexture, SDL_Texture*> select_texture : select_textures) {
    SDL_DestroyTexture(select_texture.second);
  }

  SDL_DestroyRenderer(renderer);

  SDL_DestroyWindow(window);

  IMG_Quit();

  SDL_Quit();

  return 0;
}
