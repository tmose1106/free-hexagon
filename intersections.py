from pprint import pprint


COLUMN = 9

HEX_WIDTH, HEX_HEIGHT = 64, 56


if __name__ == "__main__":
    width, height = HEX_WIDTH, HEX_HEIGHT

    half_height = height // 2

    half_width = width // 2
    three_quarter_width = width * 3 // 4

    intersections = [None for _ in range(135)]

    row_1 = [
        int(three_quarter_width + (width * 1.5) * (x // 2) + width * (x % 2))
        for x in range(9)
    ]

    column_1 = [int(height * (y + 1)) for y in range(8)]

    for i, x in enumerate(row_1):
        for j, y in enumerate(column_1):
            # print(i * 15 + j * 2, x, y)
            intersections[i * 15 + j * 2] = (x, y)

    row_2 = [
        int(width + (width * 1.5) * (x // 2) + half_width * (x % 2))
        for x in range(9)
    ]

    column_2 = [h + half_height for h in column_1[:-1]]

    for i, x in enumerate(row_2):
        for j, y in enumerate(column_2):
            # print(i * 15 + j * 2 + 1, x, y)
            intersections[i * 15 + j * 2 + 1] = (x, y)

    pprint(intersections)

